from flask import Blueprint, render_template,request, jsonify
import flask_excel as excel
import pyexcel.ext.xlsx

upload_blueprint = Blueprint('uplaod',
                             __name__,
                             url_prefix='/upload',
                             template_folder='templates')

@upload_blueprint.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        response = request.get_array(field_name='file')
        data = [[_,[]] for _ in response[0]]
        [data[i][1].append(j[i]) for j in response[1:] for i in range(len(data))]
        #data = dict([x[0], x[1]] for x in data)
        return jsonify(data)
    else:
        return render_template('upload/upload.html')
