
from flask import Blueprint, render_template

home_blueprint = Blueprint(name='home',
                 import_name=__name__,
                 url_prefix='/',
                 template_folder='templates')

@home_blueprint.route('/')
def main_page():
    return render_template('home/index.html')
