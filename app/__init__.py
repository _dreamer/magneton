from flask import Flask, render_template
from app.home import views as home
from app.upload import views as upload

app = Flask(__name__)
app.config.from_object('config')
app.register_blueprint(home.home_blueprint)
app.register_blueprint(upload.upload_blueprint)


'''@app.errorhandler(404)
def page_not_Found(404):
    render_template('404.html', 404)
'''
